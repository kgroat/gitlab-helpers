
import { CodedError } from '../errors/CodedError'
import { logger } from '../logger'
import { cleanExit } from './cleanup'

export function handleError (err: string | Error | CodedError | null | undefined) {
  if (!err) {
    logger.fatal('something went wrong')
    cleanExit(1)
    return
  }
  if (typeof err === 'string') {
    logger.fatal(err)
    cleanExit(1)
    return
  }
  const errMessage = typeof err.stack === 'string' ? err.stack : err
  logger.fatal(errMessage)
  const codedError = err as CodedError
  if (typeof codedError.code === 'number') {
    cleanExit(codedError.code)
  } else {
    cleanExit(1)
  }
}
