
import { CodedError, ErrorCode } from '../errors/CodedError'
import { logger } from '../logger'

export enum GitProtocol {
  http = 'http',
  https = 'https',
  ssh = 'ssh',
}

export interface GitUrl {
  remoteAddress: string
  protocol: GitProtocol
  host: string
  namespace: string
  project: string
  toString: () => string
  username?: string
  port?: number
}

export class GitUrlParseError extends CodedError {
  constructor (remote: string) {
    super(ErrorCode.GIT_REMOTE_PARSE_ERROR, `Could not parse remote URL '${remote}'`)
  }
}

const httpUrlParser = /^(https?):\/\/(?:([^@/]+)@)?([a-z0-9\.-]+)(?::([0-9]+))?\/(.+)\/([^\/]+)(?:\.git)$/
const sshUrlParser = /^(?:([^@/]+)@)?([a-z0-9\.-]+):(.+)\/([^\/]+)(?:\.git)$/
export function parseGitUrl (remoteAddress: string): GitUrl {
  logger.debug(`Parsing remote '${remoteAddress}'...`)
  let urlObj: GitUrl
  if (httpUrlParser.test(remoteAddress)) {
    const [, protocolStr, username, host, port, namespace, project] = httpUrlParser.exec(remoteAddress)!
    logger.debug(`Remote protocol is ${protocolStr}`)
    const protocol = GitProtocol[protocolStr] || GitProtocol.http
    urlObj = {
      remoteAddress,
      protocol,
      username: username || undefined,
      host,
      port: port ? parseInt(port) : protocol === GitProtocol.https ? 443 : 80,
      namespace,
      project,
      toString: () => remoteAddress,
    }
  } else if (sshUrlParser.test(remoteAddress)) {
    logger.debug(`Remote protocol is ssh`)
    const [, username, host, namespace, project] = sshUrlParser.exec(remoteAddress)!
    urlObj = {
      remoteAddress,
      protocol: GitProtocol.ssh,
      username,
      host,
      namespace,
      project,
      toString: () => remoteAddress,
    }
  } else {
    throw new GitUrlParseError(remoteAddress)
  }

  logger.debug('Parsed remote:', JSON.stringify(urlObj, null, 2))
  return urlObj
}

export function toBrowsableUrl (url: GitUrl): string {
  const { host, namespace, project } = url
  return `https://${host}/${namespace}/${project}`
}
