
import { runGit, GitError } from './runGit'
import { parseGitUrl, GitUrl } from '../helpers/parseGitUrl'
import { ErrorCode } from '../errors/CodedError'
import { logger } from '../logger'

export async function getRemoteUrl (remote: string = 'origin'): Promise<GitUrl> {
  const output = await runGit(['remote', 'get-url', remote], { rejectOnError: false })
  const { code, stdout, stderr } = output
  switch (code) {
    case 0:
      break
    case 128:
      throw new GitError(output, ErrorCode.GIT_INVALID_REMOTE, stderr.toString())
    default:
      throw new GitError(output, ErrorCode.GIT_ERROR, stderr.toString())
  }

  const urlStr = stdout.toString().trim()
  logger.info(`Got remote url '${urlStr}'`)
  return parseGitUrl(urlStr)
}
