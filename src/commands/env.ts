
import { Argv, Arguments } from 'yargs'
import { GitlabApi } from '../GitlabApi'
import { getRemoteUrl } from '../git/remote'
import { getCurrentBranch } from '../git/branch'
import { logger } from '../logger'
import { FishCompletion } from '../fishCompletion'
import { Environment } from '../GitlabApi/types/Environment'
import { prettyPrint } from '../helpers/prettyPrint'

interface EnvArgs {
  remote: string
  simple: boolean
  url: boolean
}

export const command = 'env [remote]'
export const describe = 'List all environments associated with the current project'

const remoteDesc = 'Optional name of remote to check'
const simpleDesc = 'Instead of tabular form, instead just print tab-separated'
const requireUrlDesc = 'Only print environments that have an external URL'

export function builder (yargs: Argv<EnvArgs>) {
  return yargs
    .positional('remote', {
      desc: remoteDesc,
      default: 'origin',
    })
    .option('simple', {
      alias: 's',
      desc: simpleDesc,
      type: 'boolean',
    })
    .option('url', {
      alias: ['require-url', 'u'],
      desc: requireUrlDesc,
      type: 'boolean',
    })
}

function printEnvironmentsTable (environments: Environment[]) {
  const tabularData = environments.map(({ name, external_url }) => {
    return [name, external_url]
  })
  tabularData.splice(0, 0, ['Name', 'External URL'])

  console.log(prettyPrint(tabularData))
}

function printEnvironmentsSimple (environments: Environment[]) {
  environments.forEach(({ name, external_url }) => {
    if (external_url) {
      console.log(`${name}\t${external_url}`)
    } else {
      console.log(name)
    }
  })
}

export async function handler (args: Arguments<EnvArgs>) {
  const url = await getRemoteUrl(args.remote)
  const {
    host,
    namespace,
    project: projectName,
  } = url
  logger.log(`remote: '${host}' '${namespace}' '${projectName}'`)
  const branch = args.branch || await getCurrentBranch()
  logger.log(`branch: '${branch}'`)

  const api = new GitlabApi(host)
  const project = await api.getProjectInNamespace(namespace, projectName)
  logger.log(`projectId: ${project.id}`)

  let environments = await api.projects.withId(project.id).environments()
  if (args.url) {
    environments = environments.filter(e => !!e.external_url)
  }
  if (args.simple) {
    printEnvironmentsSimple(environments)
  } else {
    printEnvironmentsTable(environments)
  }
}

export const fishCompletion: FishCompletion = ({
  scriptName,
  firstArgFunc,
  commandIsFunc,
  positionalArgFunc,
  gitRemoteNamesFunc,
}) => `
# ${command}
complete -f -c ${scriptName} -n '${firstArgFunc}' -a env -d '${describe}'
complete -f -c ${scriptName} -n '${commandIsFunc} env' -a '--simple' -d '${simpleDesc}'
complete -f -c ${scriptName} -n '${commandIsFunc} env' -a '--require-url' -d '${requireUrlDesc}'
complete -f -c ${scriptName} -n '${positionalArgFunc} env 1' -a '(${gitRemoteNamesFunc})' -d '${remoteDesc}'
`
