
import * as colors from 'ansi-colors'
import { argv } from './argv'
import { stringPad } from './helpers/stringPad'

export enum LogLevel {
  FATAL = 0,
  ERROR = 1,
  WARN = 2,
  LOG = 3,
  INFO = 4,
  DEBUG = 5,
  SILLY = 6,
}

export type LogLevelName = keyof typeof LogLevel

function getLogLevel (): LogLevel {
  let logLevel = LogLevel.WARN
  if (process.env.GL_LOG_LEVEL) {
    logLevel = LogLevel[process.env.GL_LOG_LEVEL.toUpperCase()]
    if (logLevel === undefined) {
      console.warn(`Tried to set logging level to "${process.env.GL_LOG_LEVEL}", which is not a valid logging level.  Defaulting to "WARN"`)
      logLevel = LogLevel.WARN
    }
  }
  if (argv.verbose) {
    logLevel = Math.min(argv.verbose + logLevel, LogLevel.SILLY)
  }
  if (logLevel > LogLevel.LOG) {
    console.error(`Running in ${LogLevel[logLevel]} mode`)
  }
  return logLevel
}

function buildLogger (level: LogLevel, baseLogger: typeof console.log, color: colors.StyleFunction = colors.reset) {
  if (LOG_LEVEL >= level) {
    const levelStr = color(stringPad(`${LogLevel[level]}:`, 6))
    if (LOG_LEVEL >= LogLevel.SILLY) {
      return (...args: any) => {
        const err = new Error()
        const [,,source] = err.stack!.split('\n')
        baseLogger(levelStr, ...args)
        baseLogger(colors.gray(source))
      }
    } else {
      return (...args: any[]) => {
        baseLogger(levelStr, ...args)
      }
    }
  } else {
    return () => null
  }
}

export const LOG_LEVEL = getLogLevel()

export const logger = {
  fatal: buildLogger(LogLevel.FATAL, console.error, colors.red.bold),
  error: buildLogger(LogLevel.ERROR, console.error, colors.red.bold),
  warn: buildLogger(LogLevel.WARN, console.warn, colors.yellow.bold),
  log: buildLogger(LogLevel.LOG, console.log, colors.green.bold),
  info: buildLogger(LogLevel.INFO, console.info, colors.cyan.bold),
  debug: buildLogger(LogLevel.DEBUG, console.debug, colors.magenta.bold),
}
