#!/usr/bin/env node

/// <reference path="./_globals.d.ts" />

import { Argv } from 'yargs'
import { handleError } from './helpers/handleError'
import { importCommands } from './commands'

export function registerCommands <T>(yargs: Argv<T>): Argv<T> {
  yargs = importCommands().reduce((yargs, cmd) => {
    return yargs.command({
      ...cmd,
      handler: (args) => {
        return new Promise(resolve => {
          setImmediate(() => {
            resolve(cmd.handler(args))
          })
        }).catch(handleError)
      }
    }) as Argv<T>
  }, yargs)

  return yargs
    .completion('comp', false)
    .demandCommand()
    .recommendCommands()
    .help()
    .strict()
}
