
import { CodedError, ErrorCode } from '../../errors/CodedError'

export class GitLabCompoundError extends CodedError {
  constructor (
    code: ErrorCode,
    message: string,
    public readonly cause?: any,
  ) {
    super(code, message)
  }
}
