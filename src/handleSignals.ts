
import { logger } from './logger'
import { cleanExit } from './helpers/cleanup'

enum Signal {
  SIGHUP = 1,
  SIGINT = 2,
  SIGQUIT = 3,
  SIGABRT = 6,
  SIGTERM = 15,
}

function exitCode(sigStr: NodeJS.Signals) {
  const baseCode = 128
  const signal = Signal[sigStr] as Signal | undefined
  if (signal !== undefined) {
    return baseCode + signal
  } else {
    return baseCode
  }
}

function registerExitHandlers(...signals: NodeJS.Signals[]) {
  signals.forEach(signal => {
    process.on(signal, () => {
      logger.warn(`received signal '${signal}' -- stopping`)
      cleanExit(exitCode(signal))
    })
  })
}

registerExitHandlers(
  'SIGHUP',
  'SIGINT',
  'SIGQUIT',
  'SIGABRT',
  'SIGTERM',
)
