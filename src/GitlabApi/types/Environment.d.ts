
import { Deployment } from './Deployment'

export interface Environment {
  id: number
  name: string
  slug: string
  external_url: string
  state?: string // 'available'
}

export interface EnvironmentDetails extends Environment {
  lastDeployment: Deployment
}
