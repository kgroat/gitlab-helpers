
import { Response } from 'node-fetch'
import { CodedError, ErrorCode } from '../errors/CodedError'

export class GitlabRequestError extends CodedError {
  constructor(
    code: ErrorCode,
    public readonly requestUrl: string,
    public readonly response: Response,
    message = `Resource '${requestUrl}' failed with code ${response.status} - ${response.statusText}`,
  ) {
    super(code, message)
  }
}
