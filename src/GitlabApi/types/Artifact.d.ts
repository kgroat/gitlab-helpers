
export interface Artifact {
  file_type: string
  size: number
  filename: string
  file_format: any
}
