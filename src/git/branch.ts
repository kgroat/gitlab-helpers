
import { runGit } from './runGit'
import { logger } from '../logger'

export async function getCurrentBranch (): Promise<string> {
  const { stdout } = await runGit(['rev-parse', '--abbrev-ref', 'HEAD'])

  const branchName = stdout.toString().trim()
  logger.info(`Got branch '${branchName}'`)
  return branchName
}
