
import { Argv, Arguments } from 'yargs'
import { GitlabApi } from '../GitlabApi'
import { getRemoteUrl } from '../git/remote'
import { getCurrentBranch } from '../git/branch'
import { logger } from '../logger'
import { toBrowsableUrl } from '../helpers/parseGitUrl'
import open from 'open'
import { FishCompletion } from '../fishCompletion'

interface MrArgs {
  branch: string
  remote: string
}

export const command = 'mr [branch] [remote]'
export const describe = 'Open the associated MR with the current git branch.'

const branchDesc = 'Optional name of the branch to check'
const remoteDesc = 'Optional name of remote to check'

export function builder (yargs: Argv<MrArgs>) {
  return yargs
    .positional('branch', {
      desc: branchDesc,
    })
    .positional('remote', {
      desc: remoteDesc,
      default: 'origin',
    })
}

export async function handler (args: Arguments<MrArgs>) {
  const url = await getRemoteUrl(args.remote)
  const {
    host,
    namespace,
    project: projectName,
  } = url
  logger.log(`remote: '${host}' '${namespace}' '${projectName}'`)
  const branch = args.branch || await getCurrentBranch()
  logger.log(`branch: '${branch}'`)

  const api = new GitlabApi(host)
  const project = await api.getProjectInNamespace(namespace, projectName)
  logger.log(`projectId: ${project.id}`)

  const mrs = await api.projects.withId(project.id).mergeRequests({
    source_branch: branch,
  })

  if (mrs.length === 0) {
    const urlStr = toBrowsableUrl(url)
    const params = new URLSearchParams({ 'merge_request[source_branch]': branch })
    const mrUrl = `${urlStr}/merge_requests/new?${params}`

    logger.log('No MRs found with the given project & source branch -- opening MR creation tool...')
    logger.info(mrUrl)
    await open(mrUrl)
  } else if (mrs.length === 1) {
    const mr = mrs[0]

    logger.log(`Opening existing MR...`)
    logger.info(mr.web_url)
    await open(mr.web_url)
  } else {
    logger.warn(`Multiple MRs found:\n - ${mrs.map(mr => mr.web_url).join('\n - ')}`)
  }
}

export const fishCompletion: FishCompletion = ({
  scriptName,
  firstArgFunc,
  positionalArgFunc,
  gitBranchNamesFunc,
  gitRemoteNamesFunc,
}) => `
# ${command}
complete -f -c ${scriptName} -n '${firstArgFunc}' -a mr -d '${describe}'
complete -f -c ${scriptName} -n '${positionalArgFunc} mr 1' -a '(${gitBranchNamesFunc})' -d '${branchDesc}'
complete -f -c ${scriptName} -n '${positionalArgFunc} mr 2' -a '(${gitRemoteNamesFunc})' -d '${remoteDesc}'
`
