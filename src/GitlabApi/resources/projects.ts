
import { Requester } from '../buildRequester'

import { buildApiEndpoint } from '../restApi/buildApiEndpoint'
import { Project } from '../types/Project'
import { MergeRequest } from '../types/MergeRequest'
import { Environment } from '../types/Environment'

export function projects(api: Requester) {
  return buildApiEndpoint({
    call: (params) => {
      return api.requestJson<Project[]>(`/projects?${params}`)
    },
    callWithId: (id, params) => {
      return api.requestJson<Project>(`/projects/${id}?${params}`)
    },
    withId: (projectId: number) => ({
      mergeRequests: buildApiEndpoint({
        call: (params) => {
          return api.requestJson<MergeRequest[]>(`/projects/${projectId}/merge_requests?${params}`)
        },
      }),
      environments: buildApiEndpoint({
        call: (params) => {
          return api.requestJson<Environment[]>(`/projects/${projectId}/environments?${params}`)
        },
        callWithId: (id, params) => {
          return api.requestJson<Environment>(`/projects/${projectId}/environments/${id}?${params}`)
        },
      }),
    })
  })
}
