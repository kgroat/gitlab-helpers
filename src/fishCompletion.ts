
import { sep } from 'path'
import { argv } from './argv'
import { importCommands, CustomCommandModule } from './commands'

export interface FishCompletionArg {
  scriptName: string
  firstArgFunc: string
  commandIsFunc: string
  positionalArgFunc: string
  gitBranchNamesFunc: string
  gitRemoteNamesFunc: string
}

export interface FishCompletion {
  (funcs: FishCompletionArg): string
}

function getFishCompletableCommands () {
  return importCommands().filter(cmd => (
    cmd.describe !== false && cmd.fishCompletion !== undefined
  )) as (CustomCommandModule & { fishCompletion: FishCompletion })[]
}

export function printAllFishCompletions () {
  const scriptPath = argv.$0.split(sep)
  const scriptName = scriptPath[scriptPath.length - 1]

  const firstArgFunc = `__fish_${scriptName}_is_first_arg`
  const commandIsFunc = `__fish_${scriptName}_command_is`
  const positionalArgFunc = `__fish_${scriptName}_positional_arg`
  const gitBranchNamesFunc = `__fish_${scriptName}_git_branches`
  const gitRemoteNamesFunc = `__fish_${scriptName}_git_remotess`

  const fishCompletionFunctions = `
  function ${firstArgFunc}
    set -l cmd (commandline -opc)
    [ (count $cmd) -eq 1 ]
  end

  function ${commandIsFunc} -a command
    set -l cmd (commandline -opc)
    [ (count $cmd) -ge 2 ]
    and [ "$cmd[2]" = "$command" ]
  end

  function ${positionalArgFunc} -a command position
    set -l cmd (commandline -opc)
    [ "$cmd[2]" = "$command" ]
    and [ (count $cmd) -eq (math 1 + $position) ]
  end

  function ${gitBranchNamesFunc}
    git branch --list --format '%(refname:short)'
  end

  function ${gitRemoteNamesFunc}
    git remote
  end
  `

  const completionArg: FishCompletionArg = {
    scriptName,
    firstArgFunc,
    commandIsFunc,
    positionalArgFunc,
    gitBranchNamesFunc,
    gitRemoteNamesFunc,
  }

  const commands = getFishCompletableCommands()
  console.log(fishCompletionFunctions)
  commands.forEach(({ fishCompletion }) => {
    console.log(fishCompletion(completionArg))
  })
}
