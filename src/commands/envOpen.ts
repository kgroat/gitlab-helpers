
import { Argv, Arguments } from 'yargs'
import { GitlabApi } from '../GitlabApi'
import { getRemoteUrl } from '../git/remote'
import { getCurrentBranch } from '../git/branch'
import { logger } from '../logger'
import { FishCompletion } from '../fishCompletion'
import open = require('open')

interface EnvArgs {
  environment: string
  remote: string
  simple: boolean
  url: boolean
}

export const command = 'env-open <environment> [remote]'
export const describe = 'List all environments associated with the current project'

const environmentDesc = 'Name of the environment to open'
const remoteDesc = 'Optional name of remote to check'

export function builder (yargs: Argv<EnvArgs>) {
  return yargs
    .positional('environment', {
      desc: environmentDesc,
    })
    .positional('remote', {
      desc: remoteDesc,
      default: 'origin',
    })
}

export async function handler (args: Arguments<EnvArgs>) {
  const url = await getRemoteUrl(args.remote)
  const {
    host,
    namespace,
    project: projectName,
  } = url
  logger.log(`remote: '${host}' '${namespace}' '${projectName}'`)
  const branch = args.branch || await getCurrentBranch()
  logger.log(`branch: '${branch}'`)

  const api = new GitlabApi(host)
  const project = await api.getProjectInNamespace(namespace, projectName)
  logger.log(`projectId: ${project.id}`)

  let environments = await api.projects.withId(project.id).environments({ name: args.environment })
  if (environments.length === 0) {
    logger.error(`Environment '${args.environment}' not found`)
    return
  }
  if (environments.length > 1) {
    logger.warn('Multiple environments found:')
    environments.forEach(env => {
      console.log(`${env.name}\t${env.external_url}`)
    })
    return
  }
  const environment = environments[0]
  if (!environment.external_url) {
    logger.warn(`Environment ${environment.name} doesn't have an associated external URL`)
    return
  }

  await open(environment.external_url)
}

export const fishCompletion: FishCompletion = ({
  scriptName,
  firstArgFunc,
  positionalArgFunc,
  gitRemoteNamesFunc,
}) => `
# ${command}
complete -f -c ${scriptName} -n '${firstArgFunc}' -a env-open -d '${describe}'
complete -f -c ${scriptName} -n '${positionalArgFunc} env-open 1' -a '(${scriptName} env -su)' -d '${environmentDesc}'
complete -f -c ${scriptName} -n '${positionalArgFunc} env-open 2' -a '(${gitRemoteNamesFunc})' -d '${remoteDesc}'
`
