
import { Project } from '../types/Project'
import { ErrorCode } from '../../errors/CodedError'
import { logger } from '../../logger'
import { GitlabApi } from '../GitlabApi'
import { GitLabCompoundError } from './GitlabCompoundError'

export interface GetProjectOpts {
  throwOnNotFound?: boolean
}

export interface GetProjectFunc {
  (namespaceStr: string, projectName: string): Promise<Project>
  (namespaceStr: string, projectName: string, opts: GetProjectOpts & { throwOnNotFound: true }): Promise<Project>
  (namespaceStr: string, projectName: string, opts: GetProjectOpts & { throwOnNotFound: false }): Promise<Project | null>
}

export function getProjectInNamespace (api: GitlabApi): GetProjectFunc {
  return async function getProjectInNamespace (namespaceStr: string, projectName: string, opts: GetProjectOpts = {}): Promise<Project | null> {
    const {
      throwOnNotFound = false,
    } = opts

    logger.info(`in getProject: '${namespaceStr}' '${projectName}'`)
    logger.debug(`Looking for namespace '${namespaceStr}'`)
    const namespace = await api.namespaces(namespaceStr)
    logger.debug(`Found namespace: ${namespace.kind} ${namespace.id}`)
    let projects: Project[]
    if (namespace.kind === 'group') {
      projects = await api.groups.withId(namespaceStr).projects({ search: projectName })
    } else if (namespace.kind === 'user') {
      projects = await api.users.withId(namespaceStr).projects({ search: projectName })
    } else {
      if (throwOnNotFound) {
        throw new GitLabCompoundError(ErrorCode.GITLAB_RESPONSE_ERROR, `Unknown namespace kind '${namespace!.kind}'`)
      } else {
        return null
      }
    }

    logger.info(`Got ${projects.length} project(s)`)
    const foundProject = projects.find(({ path, namespace: projectNS }) => (
      projectNS.id === namespace.id && path === projectName
    ))

    if (!foundProject) {
      if (throwOnNotFound) {
        throw new GitLabCompoundError(ErrorCode.GITLAB_PROJECT_NOT_FOUND, `Unable to find project '${namespaceStr}/${projectName}'`)
      } else {
        return null
      }
    }

    logger.debug(`Found project with id ${foundProject.id}`)
    return foundProject
  } as GetProjectFunc
}
