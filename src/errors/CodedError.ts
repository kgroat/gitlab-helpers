
export enum ErrorCode {
  // internal codes (0-19)
  GIT_REMOTE_PARSE_ERROR = 10,

  // git codes (20-39)
  GIT_ABORTED = 20,
  GIT_ERROR = 21,
  GIT_INVALID_REMOTE = 22,

  // gitlab codes (40-59)
  GITLAB_RESPONSE_ERROR = 40,
  GITLAB_UNAUTHORIZED = 41,
  GITLAB_FORBIDDEN = 42,
  GITLAB_NOT_FOUND = 43,
  GITLAB_TIMEOUT = 44,

  GITLAB_INVALID_HOST = 50,
  GITLAB_USER_NOT_FOUND = 51,
  GITLAB_GROUP_NOT_FOUND = 52,
  GITLAB_PROJECT_NOT_FOUND = 53,
}

export class CodedError extends Error {
  constructor(
    public readonly code: ErrorCode,
    message?: string,
  ) {
    super(message)
  }
}
