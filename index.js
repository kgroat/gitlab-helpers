#!/usr/bin/env node

require('ts-node/register')
require('./src/commands.ts').setRunnableExt('.ts')
require('./src/index.ts')
