
import fetch, { Response, RequestInit } from 'node-fetch'

import { ErrorCode } from '../errors/CodedError'
import { logger } from '../logger'
import { BASE_URI } from './restApi/constants'
import { GitlabRequestError } from './GitlabRequestError'

export interface RequesterOpts {
  remoteHost: string
  token: string | null
}

export interface Requester {
  request: (url: string, init?: RequestInit) => Promise<Response>
  requestJson: <T>(url: string, init?: RequestInit) => Promise<T>
}

export function buildRequester (opts: RequesterOpts): Requester {
  const { remoteHost, token } = opts
  const baseHeaders: { [key: string]: string } = {}
  if (token) {
    baseHeaders['Private-Token'] = token
  }
  Object.freeze(baseHeaders)

  async function request (url: string, init: RequestInit = {}): Promise<Response> {
    if (url.startsWith('/')) {
      url = url.substring(1)
    }
    if (url.endsWith('?')) {
      url = url.substring(0, url.length - 1)
    }
    const fullUrl = `https://${remoteHost}/${BASE_URI}/${url}`
    logger.info(`Making request to '${fullUrl}' ...`)
    const response = await fetch(fullUrl, {
      ...init,
      headers: {
        ...baseHeaders,
        ...init.headers,
      }
    })
    if (response.ok) {
      return response
    } else {
      logger.info(`Got an error from '${fullUrl}' - ${response.status} ${response.statusText}`)
      let code: ErrorCode = ErrorCode.GITLAB_RESPONSE_ERROR
      switch (response.status) {
        case 401: code = ErrorCode.GITLAB_UNAUTHORIZED
          break
        case 403: code = ErrorCode.GITLAB_FORBIDDEN
          break
        case 404: code = ErrorCode.GITLAB_NOT_FOUND
          break
        case 408: code = ErrorCode.GITLAB_TIMEOUT
          break
      }
      let resource = url
      if (!resource.startsWith('/')) {
        resource = `/${resource}`
      }
      if (resource.endsWith('?')) {
        resource = resource.substring(0, resource.length - 1)
      }
      throw new GitlabRequestError(code, resource, response)
    }
  }

  async function requestJson<T> (url: string, init: RequestInit = {}): Promise<T> {
    const response = await request(url, init)
    return response.json()
  }

  return {
    request,
    requestJson
  }
}
