
import { Requester } from '../buildRequester'

import { buildApiEndpoint } from '../restApi/buildApiEndpoint'
import { Namespace } from '../types/Namespace'

export function namespaces(api: Requester) {
  return buildApiEndpoint({
    call: (params) => {
      return api.requestJson<Namespace[]>(`/namespaces?${params}`)
    },
    callWithId: (id, params) => {
      return api.requestJson<Namespace>(`/namespaces/${id}?${params}`)
    }
  })
}
