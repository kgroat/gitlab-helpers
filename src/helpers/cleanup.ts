
import { gitInstances } from '../git/runGit'

export interface CleanupOpts {
  killSignal?: NodeJS.Signals
}

const DEFAULT_OPTS: CleanupOpts = { killSignal: 'SIGKILL' }

function cleanup (opts: CleanupOpts = {}) {
  const { killSignal } = { ...DEFAULT_OPTS, ...opts }

  gitInstances.forEach(child => {
    child.kill(killSignal)
  })
}

export function cleanExit (code: number, opts: CleanupOpts = {}) {
  cleanup(opts)
  process.exit(code)
}
