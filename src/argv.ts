
import * as yargs from 'yargs'
import { registerCommands } from './registerCommands'

const ygs = yargs
  .option('verbose', {
    alias: 'v',
    type: 'count',
    desc: 'Run with verbose logging\n(more times = more verbose, i.e. -vvv)'
  })

export const argv = registerCommands(ygs).argv
