
import yargs, { Argv, Arguments } from 'yargs'
import { sep } from 'path'
import { logger } from '../logger'
import { printAllFishCompletions } from '../fishCompletion'

type Shell = 'bash' | 'zsh' | 'fish'
interface CompletionArgs {
  shell: Shell
}

export const command = 'completion [shell]'
export const describe = 'Generate a completion script'
const shells: Shell[] = ['bash', 'zsh', 'fish']

function getShellName (): Shell {
  if (!process.env.SHELL) {
    return 'bash'
  }
  const shellPath = process.env.SHELL.split(sep)
  const shellName = shellPath[shellPath.length - 1] as Shell
  if (shells.includes(shellName)) {
    return shellName
  } else {
    return 'bash'
  }
}

export function builder (yargs: Argv<CompletionArgs>) {
  return yargs.positional('shell', {
    type: 'string',
    choices: shells,
    desc: 'optional shell name',
    default: getShellName(),
  }).help()
}

export async function handler (args: Arguments<CompletionArgs>) {
  if (args.shell === 'fish') {
    printAllFishCompletions()
  } else if (args.shell === 'bash' || args.shell === 'zsh') {
    process.env.SHELL = args.shell
    process.env.ZSH_NAME = args.shell
    yargs.showCompletionScript()
  } else {
    logger.error(`Unknown shell ${args.shell}`)
  }
}
