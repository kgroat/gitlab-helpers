
import { Requester } from '../buildRequester'

import { buildApiEndpoint } from '../restApi/buildApiEndpoint'
import { Project } from '../types/Project'
import { User, UserDetails } from '../types/User'

export function users(api: Requester) {
  return buildApiEndpoint({
    call: (params) => {
      return api.requestJson<User[]>(`/users?${params}`)
    },
    callWithId: (id, params) => {
      return api.requestJson<UserDetails>(`/users/${id}?${params}`)
    },
    withId: (userId: string | number) => ({
      projects: buildApiEndpoint({
        call: (params) => {
          return api.requestJson<Project[]>(`/users/${userId}/projects?${params}`)
        },
      })
    })
  })
}
