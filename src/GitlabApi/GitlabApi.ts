
import { logger } from '../logger'
import { readTokens } from '../helpers/readTokens'

import { groups } from './resources/groups'
import { namespaces } from './resources/namespaces'
import { projects } from './resources/projects'
import { users } from './resources/users'
import { buildRequester } from './buildRequester'
import { getProjectInNamespace } from './compoundResources/getProjectInNamespace'

export class GitlabApi {
  public readonly groups: ReturnType<typeof groups>
  public readonly namespaces: ReturnType<typeof namespaces>
  public readonly projects: ReturnType<typeof projects>
  public readonly users: ReturnType<typeof users>

  // Compound resources
  public readonly getProjectInNamespace = getProjectInNamespace(this)

  constructor(
    public readonly remoteHost: string,
    token?: string | null,
  ) {
    if (token === null) {
      logger.log('Null token found; not using a token')
    } else if (token === undefined) {
      logger.log('No token provided to GitlabApi')
      const tokens = readTokens()

      token = tokens[remoteHost] || null
      if (token) {
        logger.log(`Using token found for '${remoteHost}'`)
      } else {
        logger.log(`No token found for '${remoteHost}'`)
      }
    }

    const requester = buildRequester({
      remoteHost,
      token,
    })

    this.groups = groups(requester)
    this.namespaces = namespaces(requester)
    this.projects = projects(requester)
    this.users = users(requester)
  }
}
