
import { join } from 'path'
import { readdirSync } from 'fs'
import { CommandModule } from 'yargs'
import { FishCompletion } from './fishCompletion'

let runnableExt = '.js'

export interface CustomCommandModule extends CommandModule {
  fishCompletion?: FishCompletion
}

export function setRunnableExt (ext: string) {
  runnableExt = ext
}

const commandDir = join(__dirname, 'commands')
let runnableCommands: string[]
let commandModules: CustomCommandModule[]

export function getRunnableCommands () {
  if (!runnableCommands) {
    runnableCommands = readdirSync(commandDir)
      .filter(file => file.endsWith(runnableExt))
      .map(file => join(commandDir, file))
  }

  return runnableCommands
}

export function importCommands () {
  if (!commandModules) {
    commandModules = getRunnableCommands().map(cmd => require(cmd))
  }
  return commandModules
}
