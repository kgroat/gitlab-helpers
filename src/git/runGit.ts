
import { spawn, SpawnOptions, ChildProcess } from 'child_process'
import { CodedError, ErrorCode } from '../errors/CodedError'
import { logger } from '../logger'

interface GitOutput {
  code: number | null
  signal: NodeJS.Signals | null
  stdout: Buffer
  stderr: Buffer
}

interface GitOpts extends SpawnOptions {
  rejectOnError?: boolean
  rejectOnSignal?: boolean
}

export class GitError extends CodedError {
  constructor(
    public readonly output: GitOutput,
    reason: ErrorCode,
    message: string,
  ) {
    super(reason, message)
  }
}

export const gitInstances = new Set<ChildProcess>()

export function runGit(args: string[], opts: GitOpts = {}) {
  return new Promise<GitOutput>((resolve, reject) => {
    const { rejectOnError = true, rejectOnSignal = true, stdio, ...spawnOpts } = opts

    logger.info(`git ${args.map(a => a.includes(' ') ? `'${a}'` : a).join(' ')}`)
    const child = spawn('git', args, {
      ...spawnOpts,
      stdio: 'pipe',
    })

    gitInstances.add(child)

    const stdoutChunks: Buffer[] = []
    child.stdout.on('data', chunk => {
      stdoutChunks.push(chunk)
    })

    const stderrChunks: Buffer[] = []
    child.stderr.on('data', chunk => {
      stderrChunks.push(chunk)
    })

    if (stdio === 'inherit') {
      process.stdin.pipe(child.stdin)
      child.stdout.pipe(process.stdout)
      child.stderr.pipe(process.stderr)
    } else if (Array.isArray(stdio)) {
      if (stdio[0] === 'inherit') {
        process.stdin.pipe(child.stdin)
      }
      if (stdio[1] === 'inherit') {
        child.stdout.pipe(process.stdout)
      }
      if (stdio[2] === 'inherit') {
        child.stderr.pipe(process.stderr)
      }
    }

    child.on('exit', (code, signal) => {
      gitInstances.delete(child)
      const output: GitOutput = {
        code,
        signal,
        stdout: Buffer.concat(stdoutChunks),
        stderr: Buffer.concat(stderrChunks),
      }

      if (signal !== null && rejectOnSignal) {
        logger.info(`git stopped by signal ${signal}`)
        reject(new GitError(output, ErrorCode.GIT_ABORTED, `${signal} -- could not finish \`git '${args.join("', '")}'\``))
      } else if (code !== 0 && rejectOnError) {
        logger.info(`git failed with code ${code}`)
        reject(new GitError(output, ErrorCode.GIT_ERROR, `\`git '${args.join("', '")}'\` exited with error code ${code}`))
      } else {
        resolve(output)
      }
    })
  })
}
