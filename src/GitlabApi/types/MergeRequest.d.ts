import { User } from './User'

export interface MergeRequest {
  id: number
  iid: number
  project_id: number
  title: string
  description: string
  state: string // 'merged'
  /**
   * Date
   */
  created_at: string
  /**
   * Date
   */
  updated_at: string
  merged_by: User | null
  /**
   * Date
   */
  merged_at: string
  closed_by: User | null
  /**
   * Date
   */
  closed_at: string
  target_branch: string
  source_branch: string
  user_notes_count: number
  upvotes: number
  downvotes: number
  assignee: User | null
  author: User
  assignees: User[]
  source_project_id: number
  target_project_id: number
  labels: string[]
  work_in_progress: boolean
  milestone: any
  merge_when_pipeline_succeeds: boolean
  merge_status: string // 'can_be_merged'
  sha: string
  merge_commit_sha: string | null
  discussion_locked: any
  should_remove_source_branch: any
  force_remove_source_branch: boolean
  reference: string
  web_url: string
  time_stats: {
    time_estimate: number
    total_time_spent: number
    human_time_estimate: any
    human_total_time_spent: any
  }
  squash: boolean
  task_completion_status: {
    count: number
    completed_count: number
  }
  approvals_before_merge: any
}
