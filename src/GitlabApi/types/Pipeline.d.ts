
import { User } from './User'

export interface Pipeline {
  id: number
  sha: string
  ref: string
  status: string
  web_url: string
}

export interface PipelineDetails extends Pipeline {
  before_sha: string
  tag: boolean
  yaml_errors: any
  user: User
  /**
   * Date
   */
  created_at: string
  /**
   * Date
   */
  updated_at: string
  /**
   * Date
   */
  started_at: string | null
  /**
   * Date
   */
  finished_at: string | null
  /**
   * Date
   */
  committed_at: string | null
  duration: any
  coverage: string
}
