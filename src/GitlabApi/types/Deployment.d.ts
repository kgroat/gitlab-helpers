
import { User } from './User'
import { Commit } from './Commit'
import { Pipeline } from './Pipeline'
import { Artifact } from './Artifact'

export interface Deployable {
  id: number
  status: string
  stage: string
  name: string
  ref: string
  tag: boolean
  coverage: any
  created_at: string
  started_at: string
  finished_at: string
  duration: number
  user: User
  commit: Commit
  pipeline: Pipeline
  web_url: string
  artifacts: Artifact[]
  runner: any
  artifacts_expire_at: any
}

export interface Deployment {
  id: number
  iid: number
  ref: string
  sha: string
  created_at: string
  status: string
  user: User
  deployable: Deployable
}
