
import { GroupNamespace } from './Group'
import { UserNamespace } from './User'

export type Namespace = GroupNamespace | UserNamespace
