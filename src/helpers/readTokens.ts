
import { logger } from '../logger'
import { readConfig, GitlabHelperConfig } from './readConfig'

export type Tokens = NonNullable<GitlabHelperConfig['tokens']>

let tokens: Tokens | null = null

function buildTokensFromEnvVar (): Tokens | null {
  logger.debug('Trying to read tokens from GL_ACCESS_TOKENS env var...')
  const tokensStr = process.env.GL_ACCESS_TOKENS
  if (!tokensStr) {
    logger.debug('GL_ACCESS_TOKENS not set')
    return null
  }

  const tokensList = tokensStr.split(' ')
  const localTokens: Tokens = {}

  if (tokensList.length > 0) {
    tokensList.forEach(item => {
      const [remote, token] = item.split(':')
      if (remote && token) {
        localTokens[remote] = token
      }
    })

    logger.debug(`Found tokens in GL_ACCESS_TOKENS for:\n - ${Object.keys(localTokens).join('\n - ')}`)
  }

  return localTokens
}

function readTokensFromConfig (): Tokens | null {
  logger.debug('Trying to read tokens from config...')
  const config = readConfig()
  if (config === null) {
    return null
  }

  return config.tokens || null
}

export function readTokens (): Tokens {
  if (tokens === null) {
    tokens = buildTokensFromEnvVar()
  }
  if (tokens === null) {
    tokens = readTokensFromConfig()
  }
  if (tokens === null) {
    logger.debug('No tokens found')
    tokens = {}
  }

  return tokens
}
