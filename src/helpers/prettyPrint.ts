
import { stringPad } from './stringPad'

export function prettyPrint (items: string[][], spacer: string | number = '\t') {
  let spacerStr: string
  if (typeof spacer === 'number') {
    spacerStr = Array.from(Array(spacer + 1)).join(' ')
  } else {
    spacerStr = spacer
  }

  const columnWidths: number[] = []
  items.forEach(row => {
    row.forEach((cell, i) => {
      columnWidths[i] = Math.max(columnWidths[i] || 0, (cell || '').trim().length)
    })
  })

  return items
    .map(row => row.map((cell, i) => stringPad((cell || '').trim(), columnWidths[i])).join(spacerStr))
    .join('\n')
}
