#!/usr/bin/env node

/// <reference path="./_globals.d.ts" />

import 'url-search-params-polyfill'
import './handleSignals'
import './argv'
