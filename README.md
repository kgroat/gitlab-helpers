# Gitlab Helpers
A simple CLI for interacting with GitLab projects

## Table of contents
* [Installation]
* [Setup]
    * [Config file]
    * [Environment variable]
* [Usage]
    * [Commands]

[Installation]: #installation
## Installation
```bash
npm i -g gitlab-helpers
```

Or, to use it without installing:
```bash
npx gitlab-helpers
```

[Setup]: #setup
## Setup
There is a simple setup process before you can get going with the CLI.
If you want to be able to use some features, you need to set up a personal access token for the gitlab helper.
GitLab Helper needs to be able to reach the GitLab API in order to check for things such as open merge requests.

You'll need to start by [Creating a personal access token].  It needs the following permissions:
1. `api` (Required) -- Needed to access the GitLab API at all
1. `read_user` (Optional) -- Used to grab information about users & their repositories
1. `read_repository` (Optional) -- Used to interact with private repositories

You can tell GitLab Helper about your token in one of two manners:

[Config file]: #config-file
### Config file
The first option for how to register your token is to store it in a file.
The file is called `gitlab-helpers.json` and should be placed in `~/.config` (the `.config` dir under your home directory).

The file should at the very least contain a property named `tokens`, which is an object.
Each property in `tokens` should be named the DNS hostname of a gitlab server, for example `"gitlab.com"`.
The value for the property should be the personal access token used when the remote is on the given host.

For example:
```json
{
  "tokens": {
    "gitlab.com": "<your access token here>",
    "gitlab.example.com": "<another token>",
  }
}
```

When GitLab Helper runs, it attempts to validate the configuration file using a [JSON Schema].
If your editor supports it, you can validate your config file using the [provided schema].

You can have Visual Studio Code and other editors do this by adding the following to the config file:
```json
{
  "$schema": "https://gitlab.com/kgroat/gitlab-helpers/raw/master/gitlab-helper.schema.json",
  // The rest of your configuration goes here
}
```

[Environment variable]: #environment-variable
### Environment variable
The environment variable that GitLab Helper reads in order to look for personal access tokens is `GL_ACCESS_TOKENS`
The variable is an array of hosts and the associated token.
The host and token are separated by a colon (`:`).

For example:
```bash
export GL_ACCESS_TOKENS=(
  'gitlab.com:<your access token here>'
  'gitlab.example.com:<another token>'
)
```

[Usage]: #usage
## Usage
```bash
glh
```
or
```bash
gitlab-helpers
```

[Commands]: #commands
### Commands
1. `completion [shell]` -- logs out a script which can be used for adding tab-completion to the given shell
    * Supported shells are `bash`, `zsh`, and `fish`
    * If no shell is given, it defaults to the `$SHELL` environment variable
    * If `$SHELL` isn't set, or isn't a supported shell, defaults to `bash`
1. `env [remote]` -- attempts to find and print out all environments associated with the given remote
    * If no remote is given, it defaults to `origin`
1. `env-open <environment> [remote]` -- attempts to open the given environment in your browser
    * The `environment` argument is required
    * If no remote is given, it defaults to `origin`
    * If the specified environment does not have a registered External URL, an error will be printed instead
1. `mr [branch] [remote]` -- attempts to open the Merge Request associated with given branch & remote in your browser
    * If no branch is given, it defaults to the current branch
    * If no remote is given, it defaults to `origin`
    * If no Merge Request is found, it browses to the page to open a merge request
1. `open [remote]` -- attempts to open the remote repository in your browser
    * if no remote is given, it defaults to `origin`


<!-- External links -->
[JSON Schema]: https://json-schema.org/
[Creating a personal access token]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
[provided schema]: https://gitlab.com/kgroat/gitlab-helpers/raw/master/gitlab-helper.schema.json
