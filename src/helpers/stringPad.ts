
export function stringPad (str: string, minLength: number) {
  while (str.length < minLength) {
    str = `${str} `
  }
  return str
}
