
import { Requester } from '../buildRequester'

import { buildApiEndpoint } from '../restApi/buildApiEndpoint'
import { Group } from '../types/Group'
import { Project } from '../types/Project'

export function groups(api: Requester) {
  return buildApiEndpoint({
    call: (params) => {
      return api.requestJson<Group[]>(`/groups?${params}`)
    },
    callWithId: (id, params) => {
      return api.requestJson<Group>(`/groups/${id}?${params}`)
    },
    withId: (groupId: string | number) => ({
      projects: buildApiEndpoint({
        call: (params) => {
          return api.requestJson<Project[]>(`/groups/${groupId}/projects?${params}`)
        },
      })
    })
  })
}
