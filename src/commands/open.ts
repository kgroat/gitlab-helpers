
import open from 'open'
import { Argv, Arguments } from 'yargs'
import { getRemoteUrl } from '../git/remote'
import { toBrowsableUrl } from '../helpers/parseGitUrl'
import { FishCompletion } from '../fishCompletion'

interface OpenArgs {
  remote: string
}

export const command = 'open [remote]'
export const describe = 'Open the URL associated with a given remote.'
const remoteDesc = 'optional name of remote to check'

export function builder (yargs: Argv<OpenArgs>) {
  return yargs.positional('remote', {
    desc: remoteDesc,
    default: 'origin',
  })
}

export async function handler (args: Arguments<OpenArgs>) {
  const urlObj = await getRemoteUrl(args.remote)
  const urlStr = toBrowsableUrl(urlObj)

  console.log(`Opening ${urlStr} ...`)
  await open(urlStr)
}

export const fishCompletion: FishCompletion = ({
  scriptName,
  firstArgFunc,
  positionalArgFunc,
  gitRemoteNamesFunc,
}) => `
# ${command}
complete -f -c ${scriptName} -n '${firstArgFunc}' -a open -d '${describe}'
complete -f -c ${scriptName} -n '${positionalArgFunc} open 1' -a '(${gitRemoteNamesFunc})' -d '${remoteDesc}'
`
