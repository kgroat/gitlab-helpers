
import { join } from 'path'
import { readFileSync, statSync } from 'fs'
import Ajv, { ValidateFunction } from 'ajv'
import { logger } from '../logger'

const jsonValidator = new Ajv({
  allErrors: true,
  logger,
  async: false,
  // verbose: LOG_LEVEL >= LogLevel.INFO,
})

export interface GitlabHelperConfig {
  tokens?: {
    [host: string]: string | undefined
  }
}

let validateGitlabConfig: ValidateFunction
let config: GitlabHelperConfig | null

function getConfigValidator (): ValidateFunction {
  if (!validateGitlabConfig) {
    const schema = require(join(__dirname, '../../gitlab-helper.schema.json'))
    validateGitlabConfig = jsonValidator.compile(schema)
  }

  validateGitlabConfig.errors = null

  return validateGitlabConfig
}

function readConfigFromFile (source: string): GitlabHelperConfig | null {
  logger.debug(`Trying to read config from '${source}'...`)
  let localConfig: GitlabHelperConfig | null = null
  try {
    const stats = statSync(source)
    if (!stats.isFile()) {
      logger.debug(`'${source}' is not a file`)
      return null
    }
  } catch {
    logger.debug(`Config file '${source}' does not exist.`)
    return null
  }

  try {
    const fileData = readFileSync(source)
    localConfig = JSON.parse(fileData.toString())
  } catch (e) {
    logger.debug(`Failed to read config file '${source}' --`, e)
    return null
  }

  if (localConfig) {
    logger.debug('Config file successfully loaded')
    logger.log('Validating config...')
    try {
      const validateConfig = getConfigValidator()
      const valid = validateConfig(localConfig)
      if (valid) {
        logger.log('Validation passed')
      } else {
        logger.warn(`Config file '${source}' failed validation:`)
        if (validateConfig.errors) {
          validateConfig.errors.forEach(({ dataPath, message }) => {
            logger.warn(`${dataPath} -- ${message}`)
          })
        }
      }
    } catch (e) {
    }
  }

  return localConfig
}

export function readConfig (): GitlabHelperConfig | null {
  if (!config) {
    config = readConfigFromFile(join(process.env.HOME || '~', '.config/gitlab-helpers.json'))
  }

  return config
}
