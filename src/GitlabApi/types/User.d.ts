
interface UserCommon {
  id: number
  name: string
  username: string
  state: string // 'active'
  avatar_url: string
  web_url: string
}

export interface UserNamespace extends UserCommon {
  kind: 'user'
  full_path: string
  plan: string // 'free'
}

export interface User extends UserCommon {
}

export interface UserDetails extends User {
  /**
   * Date
   */
  created_at: string
  bio: string
  location: string
  public_email: string
  skype: string
  linkedin: string
  twitter: string
  website_url: string
  organization: string
}
