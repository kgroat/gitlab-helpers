
interface Params {
  [name: string]: string
}

export interface ApiEndpointConfig {
  call: (params: URLSearchParams) => any
  callWithId?: (id: string | number, params: URLSearchParams) => any
  [subcommand: string]: any
}

type BuiltCall<Cfg extends ApiEndpointConfig> =
  Cfg extends { callWithId: Function }
  ? ((id: string | number | null, params?: Params) => ReturnType<Cfg['callWithId']>) & ((params?: Params) => ReturnType<Cfg['call']>)
  : ((params?: Params) => ReturnType<Cfg['call']>)

type BuiltSubcommands<Cfg extends ApiEndpointConfig> = {
  [s in Exclude<keyof Cfg, 'call' | 'callWithId'>]: Cfg[s]
}

export type BuiltEndpoint<Cfg extends ApiEndpointConfig> =
  BuiltCall<Cfg> & BuiltSubcommands<Cfg>

export function buildApiEndpoint<Cfg extends ApiEndpointConfig> (config: Cfg): BuiltEndpoint<Cfg> {
  const { call, callWithId, ...subcommands } = config
  let output: BuiltEndpoint<Cfg>
  if (callWithId) {
    output = ((id?: string | number | Params, params?: Params) => {
      if (typeof id === 'string' || typeof id === 'number') {
        return callWithId(id, new URLSearchParams(params))
      } else {
        params = id
        return call(new URLSearchParams(params))
      }
    }) as BuiltEndpoint<Cfg>
  } else {
    output = ((params?: Params) => {
      return call(new URLSearchParams(params))
    }) as BuiltEndpoint<Cfg>
  }

  const keys = Object.keys(subcommands) as (keyof typeof subcommands)[]
  keys.forEach(sub => {
    output[sub] = subcommands[sub] as any
  })

  return output
}
