import { Project } from './Project'

interface GroupCommon {
  id: number
  web_url: string
  name: string
  path: string
  full_path: string
  parent_id: number | null
  avatar_url: string | null
}

export interface GroupNamespace extends GroupCommon {
  kind: 'group'
}

export interface Group extends GroupCommon {
  description: string
  visibility: string // 'public'
  lfs_enabled: boolean
  request_access_enabled: boolean
  full_name: string
  projects: Project[]
  shared_projects: Project[]
  ldap_cn: string | null
  ldap_access: null
  file_template_project_id: number | null
  shared_runners_minutes_limit: number | null
  extra_shared_runners_minutes_limit: number | null
}
