import { Namespace } from './Namespace'


export interface Project {
  id: number
  description: string
  name: string
  name_with_namespace: string
  path: string
  path_with_namespace: string
  /**
   * Date
   */
  created_at: string
  default_branch: string
  tag_list: string[],
  ssh_url_to_repo: string
  http_url_to_repo: string
  web_url: string
  readme_url: string
  avatar_url: string | null
  star_count: number
  forks_count: number
  /**
   * Date
   */
  last_activity_at: string
  namespace: Namespace,
  _links: {
    self: string
    issues: string
    merge_requests: string
    repo_branches: string
    labels: string
    events: string
    members: string
  }
  empty_repo: boolean
  archived: boolean
  visibility: string // 'public'
  resolve_outdated_diff_discussions: null
  container_registry_enabled: null
  issues_enabled: boolean
  merge_requests_enabled: boolean
  wiki_enabled: boolean
  jobs_enabled: boolean
  snippets_enabled: boolean
  issues_access_level: string // 'enabled' | 'disabled'
  repository_access_level: string // 'enabled' | 'disabled'
  merge_requests_access_level: string // 'enabled' | 'disabled'
  wiki_access_level: string // 'enabled' | 'disabled'
  builds_access_level: string // 'enabled' | 'disabled'
  snippets_access_level: string // 'enabled' | 'disabled'
  shared_runners_enabled: boolean
  lfs_enabled: boolean
  creator_id: number
  import_status: string // 'none'
  open_issues_count: number
  ci_default_git_depth: null
  public_jobs: boolean
  build_timeout: number
  auto_cancel_pending_pipelines: string // 'enabled' | 'disabled'
  build_coverage_regex: string | null
  ci_config_path: string | null
  shared_with_groups: any[],
  only_allow_merge_if_pipeline_succeeds: boolean
  request_access_enabled: boolean
  only_allow_merge_if_all_discussions_are_resolved: null
  printing_merge_request_link_enabled: boolean
  merge_method: string // 'merge'
  auto_devops_enabled: string
  auto_devops_deploy_strategy: string // 'continuous'
  approvals_before_merge: number
  mirror: boolean
  external_authorization_classification_label: null
  packages_enabled: null
}
